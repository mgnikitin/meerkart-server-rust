FROM rust:1.54.0-slim-buster AS build
RUN apt-get update && apt-get install -y libssl-dev pkg-config

WORKDIR /usr/src/geomap
COPY . .

RUN cargo install --path app

FROM debian:buster-slim AS runtime
RUN apt-get update && apt-get install -y libssl1.1 && rm -rf /var/lib/apt/lists/*
COPY --from=build /usr/local/cargo/bin/app /usr/local/bin/app
RUN mkdir -p /tmp/geomap
COPY --from=build /usr/src/geomap/app/config/conf.toml /etc/app/conf.toml
ENTRYPOINT ["/usr/local/bin/app", "-c", "/etc/app/conf.toml"]