# Generate server API
```
docker run --rm -v /home/user/qt_projects/geomap-rust:/local openapitools/openapi-generator-cli generate -i /local/server/spec/tilestore.yml -g rust-server -o /local/server -c /local/server/spec/config.yml
```

# Build Docker image
```
docker build -t tile-server-rust .
```

# Run Docker container
```
docker run -p 8080:8080 --rm -d -v /mnt/geomap-data:/tmp/geomap --name tile-server-rust-cont tile-server-rust
```