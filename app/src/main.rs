#![allow(missing_docs)]

mod memorypool;
mod server;
mod settings;
mod tiledata;
mod tilefilecache;
mod tilefilestorage;
mod tilestorage;
mod util;

use clap::{App, Arg};
use std::path::{Path,PathBuf};
use settings::Settings;

#[tokio::main]
async fn main() {
    env_logger::init();
    let matches = App::new("server")
        .arg(Arg::with_name("https")
                .long("https")
                .help("Whether to use HTTPS or not"),
        )
        .arg(Arg::with_name("config")
            .short("c")
            .long("config")
            .takes_value(true)
            .help("Configuration file"),
        )
        .get_matches();

    let addr = "127.0.0.1:8080";
    let path = match matches.value_of("config") {
        Some(p) => PathBuf::from(p),
        None => Path::new("config").join("conf.toml")
    };
    let s = Settings::new(&path).expect("Unable to parse config");

    server::create(addr, matches.is_present("https"), &s).await;
}
