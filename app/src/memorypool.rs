use std::collections::VecDeque;

struct Element<T>
where
    T: Default,
{
    d: T,
    filled: bool,
}

pub struct MemoryPool<T>
where
    T: Default,
{
    data: VecDeque<Element<T>>,
    available: VecDeque<usize>,
}

impl<T> MemoryPool<T>
where
    T: Default,
{
    pub fn new() -> Self {
        MemoryPool {
            data: VecDeque::new(),
            available: VecDeque::new(),
        }
    }

    pub fn data_mut(&mut self, idx: usize) -> &mut T {
        match self.data.get_mut(idx) {
            Some(d) => &mut d.d,
            None => panic!("Out of bounds access"),
        }
    }

    pub fn data(&self, idx: usize) -> &T {
        match self.data.get(idx) {
            Some(d) => &d.d,
            None => panic!("Out of bounds access"),
        }
    }

    pub fn put(&mut self, d: T) -> usize {
        match self.available.back() {
            Some(idx) => {
                assert!(!self.data[*idx].filled);
                self.data[*idx].d = d;
                self.data[*idx].filled = true;
                let ret = *idx;
                self.available.pop_back();
                ret
            }
            None => {
                let idx = self.data.len();
                self.data.push_back(Element { d: d, filled: true });
                idx
            }
        }
    }

    pub fn release(&mut self, idx: usize) -> T {
        assert!(self.data.len() > idx);
        assert!(self.data[idx].filled);

        let t = std::mem::replace(
            &mut self.data[idx],
            Element {
                d: T::default(),
                filled: false,
            },
        );
        self.available.push_back(idx);
        t.d
    }
}
