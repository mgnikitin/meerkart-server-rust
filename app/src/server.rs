#![allow(unused_imports)]

use async_trait::async_trait;
use futures::{future, Stream, StreamExt, TryFutureExt, TryStreamExt};
use hyper::server::conn::Http;
use hyper::service::Service;
use log::info;
use std::future::Future;
use std::marker::PhantomData;
use std::net::SocketAddr;
use std::pin::Pin;
use std::sync::{Arc, LockResult, Mutex};
use std::task::{Context, Poll};
use swagger::auth::MakeAllowAllAuthenticator;
use swagger::EmptyContext;
use swagger::{Has, XSpanIdString};
use tokio::net::TcpListener;
use tokio_openssl::SslStream;

#[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
use openssl::ssl::{Ssl, SslAcceptor, SslFiletype, SslMethod};

use openapi::models;

extern crate proto;

use crate::settings::Settings;
use crate::tiledata::{TileData, TileId};
use crate::tilestorage::TileStorage;
use protobuf::Message;
use rtree::common::{Rect, Vertex2};
use std::convert::TryFrom;
use std::path::Path;
use units::distance::Meter;

trait LockResultExt {
    type Guard;

    fn ignore_poision(self) -> Self::Guard;
}

impl<Guard> LockResultExt for LockResult<Guard> {
    type Guard = Guard;

    fn ignore_poision(self) -> Guard {
        self.unwrap_or_else(|e| e.into_inner())
    }
}

/// Builds an SSL implementation for Simple HTTPS from some hard-coded file names
pub async fn create(addr: &str, https: bool, settings: &Settings) {
    let addr = addr.parse().expect("Failed to parse bind address");
    let tile_storage = Arc::new(Mutex::new(TileStorage::new(
        settings.storage.directory.as_path(),
    )));
    let server = Server::new(tile_storage.clone());

    let service = MakeService::new(server);

    let service = MakeAllowAllAuthenticator::new(service, "cosmo");

    let mut service = openapi::server::context::MakeAddContext::<_, EmptyContext>::new(service);

    if https {
        #[cfg(any(target_os = "macos", target_os = "windows", target_os = "ios"))]
        {
            unimplemented!("SSL is not implemented for the examples on MacOS, Windows or iOS");
        }

        #[cfg(not(any(target_os = "macos", target_os = "windows", target_os = "ios")))]
        {
            let mut acceptor = SslAcceptor::mozilla_intermediate_v5(SslMethod::tls())
                .expect("Failed to create SSL Acceptor");

            // Server authentication
            acceptor
                .set_private_key_file("examples/server-key.pem", SslFiletype::PEM)
                .expect("Failed to set private key");
            acceptor
                .set_certificate_chain_file("examples/server-chain.pem")
                .expect("Failed to set cerificate chain");
            acceptor
                .check_private_key()
                .expect("Failed to check private key");

            let acceptor = Arc::new(acceptor.build());
            let tcp_listener = TcpListener::bind(&addr).await.unwrap();

            loop {
                let (socket, addr) = tcp_listener.accept().await.unwrap();
                let service = service.call(addr);
                let acceptor = Arc::clone(&acceptor);

                tokio::spawn(async move {
                    let ssl = Ssl::new(acceptor.context()).unwrap();
                    let mut stream = SslStream::new(ssl, socket).unwrap();
                    Pin::new(&mut stream).accept().await.unwrap();
                    let service = service.await.map_err(|_| ())?;

                    Http::new()
                        .serve_connection(stream, service)
                        .await
                        .map_err(|_| ())
                });
            }
        }
    } else {
        // Using HTTP
        hyper::server::Server::bind(&addr)
            .http1_title_case_headers(true)
            .http2_max_frame_size(10485760)
            .serve(service)
            .await
            .unwrap()
    }
}

//#[derive(Copy, Clone)]
#[derive(Clone)]
pub struct Server<C> {
    marker: PhantomData<C>,
    tile_storage: Arc<Mutex<TileStorage>>,
}

impl<C> Server<C> {
    pub fn new(tile_storage: Arc<Mutex<TileStorage>>) -> Self {
        Server {
            marker: PhantomData,
            tile_storage: tile_storage,
        }
    }
}

use openapi::server::MakeService;
use openapi::{
    AddTileResponse, Api, DeleteTileResponse, GetTileDataByIdResponse, GetTilesByRectResponse,
    GetVersionResponse,
};
use std::error::Error;
use swagger::ApiError;

#[async_trait]
impl<C> Api<C> for Server<C>
where
    C: Has<XSpanIdString> + Send + Sync,
{
    /// Add tile data
    async fn add_tile(
        &self,
        tile_data: Option<swagger::ByteArray>,
        context: &C,
    ) -> Result<AddTileResponse, ApiError> {
        let context = context.clone();
        info!("add_tile - X-Span-ID: {:?}", context.get().0.clone());
        match tile_data {
            Some(data) => {
                let mut proto: proto::tiledata::TileData = Message::parse_from_bytes(&data)
                    .map_err(|e| ApiError(format!("Protobuf error: {}", e.to_string())))?;
                let td = TileData::try_from(&mut proto)
                    .map_err(|e| ApiError(format!("Convert error: {}", e.to_string())))?;
                let mut tile_storage = self.tile_storage.lock().ignore_poision();
                let id = tile_storage.put(td);
                Ok(AddTileResponse::Added(models::TileId {
                    id: i64::try_from(id.idx).ok(),
                }))
            }
            None => Ok(AddTileResponse::InvalidInput),
        }
    }

    /// Delete a tile
    async fn delete_tile(&self, id: i64, context: &C) -> Result<DeleteTileResponse, ApiError> {
        let context = context.clone();
        info!(
            "delete_tile({}) - X-Span-ID: {:?}",
            id,
            context.get().0.clone()
        );
        Err(ApiError("Generic failuare".to_owned()))
    }

    /// Returns a tile data
    async fn get_tile_data_by_id(
        &self,
        id: i64,
        context: &C,
    ) -> Result<GetTileDataByIdResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_tile_data_by_id({}) - X-Span-ID: {:?}",
            id,
            context.get().0.clone()
        );

        let id = TileId::new(id as u64);
        let mut tile_storage = self.tile_storage.lock().ignore_poision();
        let td = tile_storage.get_data(id);
        let td = proto::tiledata::TileData::from(td);
        let bytes = swagger::ByteArray(
            td.write_to_bytes()
                .map_err(|e| ApiError(format!("Protobuf error: {}", e.to_string())))?,
        );
        Ok(GetTileDataByIdResponse::ABinaryObjectOfTile(bytes))
    }

    /// Returns a list of tiles
    async fn get_tiles_by_rect(
        &self,
        left: i64,
        right: i64,
        bottom: i64,
        top: i64,
        zoom_level: Option<i32>,
        context: &C,
    ) -> Result<GetTilesByRectResponse, ApiError> {
        let context = context.clone();
        info!(
            "get_tiles_by_rect({}, {}, {}, {}, {:?}) - X-Span-ID: {:?}",
            left,
            right,
            bottom,
            top,
            zoom_level,
            context.get().0.clone()
        );
        let rect = Rect::new(
            Vertex2::new(Meter::new(left as f64), Meter::new(bottom as f64)),
            Vertex2::new(Meter::new(right as f64), Meter::new(top as f64)),
        );
        let zoom_level = match zoom_level {
            Some(z) => z as u32,
            None => 0,
        };
        let tile_storage = self.tile_storage.lock().ignore_poision();
        let vec = tile_storage
            .find(&rect)
            .into_iter()
            .filter(|v| (v.zoom_level + 1) >= zoom_level && v.zoom_level <= (zoom_level + 2))
            .map(|v| openapi::models::TileInfo {
                id: Some(openapi::models::TileId {
                    id: Some(v.id.idx as i64),
                }),
                revision: Some(v.revision as usize),
                provider_id: Some(v.provider_id),
                data_key: Some(v.data_key),
                zoom_level: Some(v.zoom_level as usize),
                border_rect: Some(openapi::models::BorderRect {
                    bottom_left: Some(openapi::models::Vertex2 {
                        x: Some(v.border_rect.bl.x.value),
                        y: Some(v.border_rect.bl.y.value),
                    }),
                    top_right: Some(openapi::models::Vertex2 {
                        x: Some(v.border_rect.tr.x.value),
                        y: Some(v.border_rect.tr.y.value),
                    }),
                }),
            })
            .collect::<Vec<_>>();
        Ok(GetTilesByRectResponse::AJSONArrayOfTilesWithInfo(vec))
    }

    /// Get API version
    async fn get_version(&self, context: &C) -> Result<GetVersionResponse, ApiError> {
        let context = context.clone();
        info!("get_version() - X-Span-ID: {:?}", context.get().0.clone());
        Ok(GetVersionResponse::ObjectWithCurrentVersionString(
            openapi::models::InlineResponse200 {
                current_version: Some(openapi::API_VERSION.to_owned()),
            },
        ))
    }
}
