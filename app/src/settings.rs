use config::{Config, ConfigError, File};
use serde::Deserialize;
use std::path::{Path, PathBuf};

#[derive(Debug, Deserialize)]
pub struct FileStorage {
    pub directory: PathBuf,
}

#[derive(Debug, Deserialize)]
pub struct Settings {
    pub storage: FileStorage,
}

impl Settings {
    pub fn new(path: &Path) -> Result<Self, ConfigError> {
        let mut s = Config::default();
        let path = path
            .to_str()
            .ok_or(ConfigError::Message("Unable to make path".to_owned()))?;
        s.merge(File::with_name(path))?;

        s.try_into()
    }
}
