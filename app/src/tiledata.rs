use itertools::Itertools;
use rtree::common::{Rect, Vertex2};
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;
use std::convert::{From, TryFrom};
use std::fmt;
use units::distance::Meter;

#[derive(Copy, Clone, Default, Serialize, Deserialize, Eq, PartialEq, Debug, Hash)]
pub struct TileId {
    pub idx: u64,
}

impl TileId {
    pub fn new(idx: u64) -> TileId {
        TileId { idx: idx }
    }
}

impl fmt::Display for TileId {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.idx)
    }
}

type Vertex2m = Vertex2<Meter>;
type Vertex2f = Vertex2<f32>;
type Rect2m = Rect<Meter>;

fn decode_vertex2f(a: proto::tiledata::Vertex2f) -> Vertex2f {
    Vertex2f {
        x: a.get_x(),
        y: a.get_y(),
    }
}

fn encode_vertex2f(a: Vertex2f) -> proto::tiledata::Vertex2f {
    let mut ret = proto::tiledata::Vertex2f::default();
    ret.set_x(a.x);
    ret.set_y(a.y);
    ret
}

fn decode_vertex2m(a: proto::tiledata::Vertex2m) -> Vertex2m {
    let x = a.get_x();
    let y = a.get_y();
    Vertex2m {
        x: Meter { value: x },
        y: Meter { value: y },
    }
}

fn encode_vertex2m(a: Vertex2m) -> proto::tiledata::Vertex2m {
    let mut ret = proto::tiledata::Vertex2m::default();
    ret.set_x(a.x.value);
    ret.set_y(a.y.value);
    ret
}

fn decode_rect(mut a: proto::tiledata::Rect2m) -> Rect2m {
    let bl = decode_vertex2m(a.take_bl());
    let tr = decode_vertex2m(a.take_tr());
    Rect2m { bl: bl, tr: tr }
}

fn encode_rect(a: Rect2m) -> proto::tiledata::Rect2m {
    let mut ret = proto::tiledata::Rect2m::default();
    ret.set_bl(encode_vertex2m(a.bl));
    ret.set_tr(encode_vertex2m(a.tr));
    ret
}

fn convert_vec<T, U: From<T>>(vec: Vec<T>) -> Vec<U> {
    vec.into_iter().map(|v| U::from(v)).collect::<Vec<_>>()
}

fn convert_vecdeq<T, U, F: FnMut(T) -> U>(vec: Vec<T>, f: F) -> VecDeque<U> {
    vec.into_iter().map(f).collect::<VecDeque<_>>()
}

fn convert_deqvec<T, U, F: FnMut(T) -> U>(vec: VecDeque<T>, f: F) -> Vec<U> {
    vec.into_iter().map(f).collect::<Vec<_>>()
}

#[derive(Clone, Default, Serialize, Deserialize, PartialEq, Debug)]
pub struct TileInfo {
    pub id: TileId,
    pub revision: u32,
    pub provider_id: String,
    pub data_key: String,
    pub zoom_level: u32,
    pub border_rect: Rect2m,
}

impl TryFrom<&mut proto::tiledata::TileInfo> for TileInfo {
    type Error = &'static str;
    fn try_from(a: &mut proto::tiledata::TileInfo) -> Result<Self, Self::Error> {
        let border_rect = decode_rect(a.take_border_rect());
        Ok(TileInfo {
            id: TileId { idx: a.id },
            revision: a.revision,
            provider_id: a.provider_id.clone(),
            data_key: a.data_key.clone(),
            zoom_level: a.zoom_level,
            border_rect: border_rect,
        })
    }
}

impl From<TileInfo> for proto::tiledata::TileInfo {
    fn from(a: TileInfo) -> Self {
        let mut ti = proto::tiledata::TileInfo::default();
        ti.set_id(a.id.idx);
        ti.set_revision(a.revision);
        ti.set_provider_id(a.provider_id);
        ti.set_data_key(a.data_key);
        ti.set_zoom_level(a.zoom_level);
        ti.set_border_rect(encode_rect(a.border_rect));
        ti
    }
}

#[derive(Clone, Default, PartialEq, Serialize, Deserialize, Debug)]
pub struct Property {
    name: String,
    value: String,
}

impl From<proto::tiledata::Property> for Property {
    fn from(a: proto::tiledata::Property) -> Self {
        Property {
            name: a.name.clone(),
            value: a.value.clone(),
        }
    }
}

impl From<Property> for proto::tiledata::Property {
    fn from(a: Property) -> Self {
        let mut ret = proto::tiledata::Property::default();
        ret.set_name(a.name);
        ret.set_value(a.value);
        ret
    }
}

#[derive(Clone, Default, PartialEq, Serialize, Deserialize, Debug)]
pub struct Point {
    pub coord: Vertex2f,
    pub properties: Vec<Property>,
}

impl From<proto::tiledata::Point> for Point {
    fn from(mut a: proto::tiledata::Point) -> Self {
        Point {
            coord: decode_vertex2f(a.take_coord()),
            properties: convert_vec(a.take_properties().into_vec()),
        }
    }
}

impl From<Point> for proto::tiledata::Point {
    fn from(a: Point) -> Self {
        let mut ret = proto::tiledata::Point::default();
        ret.set_coord(encode_vertex2f(a.coord));
        ret
    }
}

#[derive(Clone, Default, PartialEq, Serialize, Deserialize, Debug)]
pub struct CoordVec {
    pub data: VecDeque<Vertex2f>,
}

impl TryFrom<proto::tiledata::CoordArray> for CoordVec {
    type Error = &'static str;
    fn try_from(a: proto::tiledata::CoordArray) -> Result<Self, Self::Error> {
        match a.data.len() % 2 {
            0 => Ok(CoordVec {
                data: a
                    .data
                    .into_iter()
                    .tuples()
                    .map(|(x, y)| Vertex2f::new(x, y))
                    .collect::<VecDeque<_>>(),
            }),
            _ => Err("Invalid data len"),
        }
    }
}

impl From<CoordVec> for proto::tiledata::CoordArray {
    fn from(a: CoordVec) -> Self {
        let mut vec = Vec::new();
        for v in a.data {
            vec.push(v.x);
            vec.push(v.y);
        }
        let mut ret = proto::tiledata::CoordArray::default();
        ret.set_data(vec);
        ret
    }
}

#[derive(Clone, Default, PartialEq, Serialize, Deserialize, Debug)]
pub struct Line {
    pub coords: CoordVec,
    pub properties: Vec<Property>,
}

impl From<proto::tiledata::Line> for Line {
    fn from(mut a: proto::tiledata::Line) -> Self {
        Line {
            coords: CoordVec {
                data: convert_vecdeq(a.take_coords().into_vec(), |v| decode_vertex2f(v)),
            },
            properties: convert_vec(a.take_properties().into_vec()),
        }
    }
}

impl From<Line> for proto::tiledata::Line {
    fn from(a: Line) -> Self {
        let mut ret = proto::tiledata::Line::default();
        ret.set_coords(protobuf::RepeatedField::from_vec(convert_deqvec(
            a.coords.data,
            |v| encode_vertex2f(v),
        )));
        ret.set_properties(protobuf::RepeatedField::from_vec(convert_vec(a.properties)));
        ret
    }
}

#[derive(Clone, Default, PartialEq, Serialize, Deserialize, Debug)]
pub struct Area {
    pub outer: CoordVec,
    pub inners: VecDeque<CoordVec>,
    pub properties: Vec<Property>,
}

impl TryFrom<proto::tiledata::Area> for Area {
    type Error = &'static str;
    fn try_from(mut a: proto::tiledata::Area) -> Result<Self, Self::Error> {
        let outer = CoordVec::try_from(a.take_outer())?;
        let inners = a
            .take_inners()
            .into_vec()
            .into_iter()
            .map(|v| CoordVec::try_from(v))
            .collect::<Result<VecDeque<CoordVec>, _>>()?;
        Ok(Area {
            outer: outer,
            inners: inners,
            properties: convert_vec(a.take_properties().into_vec()),
        })
    }
}

impl From<Area> for proto::tiledata::Area {
    fn from(a: Area) -> Self {
        let mut ret = proto::tiledata::Area::default();
        ret.set_outer(proto::tiledata::CoordArray::from(a.outer));
        ret.set_inners(protobuf::RepeatedField::from_vec(
            a.inners
                .into_iter()
                .map(|v| proto::tiledata::CoordArray::from(v))
                .collect(),
        ));
        ret.set_properties(protobuf::RepeatedField::from_vec(convert_vec(a.properties)));
        ret
    }
}

#[derive(Clone, Default, PartialEq, Serialize, Deserialize, Debug)]
pub struct TileData {
    pub tile_info: TileInfo,
    // basis for coords
    pub origin: Vertex2m,
    // points, related coords
    pub points: VecDeque<Point>,
    // lines, related coords
    pub lines: VecDeque<Line>,
    // areas, related coords
    pub areas: VecDeque<Area>,
}

impl TryFrom<&mut proto::tiledata::TileData> for TileData {
    type Error = &'static str;
    fn try_from(a: &mut proto::tiledata::TileData) -> Result<Self, Self::Error> {
        let tile_info = TileInfo::try_from(a.mut_info())?;
        let areas = a
            .take_areas()
            .into_vec()
            .into_iter()
            .map(|v| Area::try_from(v))
            .collect::<Result<VecDeque<Area>, _>>()?;
        Ok(TileData {
            tile_info: tile_info,
            origin: decode_vertex2m(a.take_origin()),
            points: convert_vecdeq(a.take_points().into_vec(), |v| v.into()),
            lines: convert_vecdeq(a.take_lines().into_vec(), |v| v.into()),
            areas: areas,
        })
    }
}

impl From<TileData> for proto::tiledata::TileData {
    fn from(a: TileData) -> Self {
        let mut td = proto::tiledata::TileData::default();
        td.set_info(a.tile_info.into());
        td.set_origin(encode_vertex2m(a.origin));
        td.set_points(protobuf::RepeatedField::from_vec(convert_deqvec(
            a.points,
            |v| v.into(),
        )));
        td.set_lines(protobuf::RepeatedField::from_vec(convert_deqvec(
            a.lines,
            |v| v.into(),
        )));
        td.set_areas(protobuf::RepeatedField::from_vec(convert_deqvec(
            a.areas,
            |v| v.into(),
        )));
        td
    }
}
