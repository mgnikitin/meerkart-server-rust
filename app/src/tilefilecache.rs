use crate::tiledata::{TileData, TileId, TileInfo};
use hex;
use log::info;
use rtree::common::{Rect, Vertex2};
use std::collections::HashMap;
use std::convert::From;
use std::convert::TryInto;
use std::fmt;
use std::fs::{self, OpenOptions};
use std::path::{Path, PathBuf};
use units::distance::Meter;

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct FileKey {
    pub key: String,
}

impl From<&TileInfo> for FileKey {
    fn from(ti: &TileInfo) -> FileKey {
        FileKey {
            key: format!("{}-{}", ti.provider_id, ti.data_key),
        }
    }
}

impl fmt::Display for FileKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.key)
    }
}

struct Meta {
    filename: String,
    tile_info: TileInfo,
}

pub struct TileFileCache {
    directory: PathBuf,
    cache: HashMap<FileKey, Meta>,
}

impl TileFileCache {
    pub fn new(directory: &Path) -> Self {
        TileFileCache {
            directory: PathBuf::from(directory),
            cache: HashMap::new(),
        }
    }

    pub fn put(&mut self, tile_data: &TileData) -> FileKey {
        let file_key = FileKey::from(&tile_data.tile_info);
        let filename = tile_info_to_filename(&tile_data.tile_info);
        let meta = Meta {
            filename: filename,
            tile_info: tile_data.tile_info.clone(),
        };
        let filepath = self.directory.as_path();
        let filepath = filepath.join(Path::new(&meta.filename));

        info!("TileFileCache: put {}", filepath.display());
        let f = OpenOptions::new().write(true).create(true).open(filepath);
        let f = match f {
            Ok(file) => file,
            Err(err) => panic!("Unable to open file for writing: {:?}", err),
        };
        serde_cbor::to_writer(f, &tile_data).expect("Unable to serialize to file");
        self.cache.insert(file_key.clone(), meta);
        file_key
    }

    pub fn remove(&mut self, file_key: &FileKey) {
        let item = self
            .cache
            .remove(&file_key)
            .unwrap_or_else(|| panic!("Unable to find key: {}", file_key));

        let filepath = self.directory.as_path();
        let filepath = filepath.join(item.filename);
        info!("TileFileCache: remove {}", filepath.display());
        fs::remove_file(filepath).unwrap_or_else(|err| panic!("Unable to remove file: {}", err));
    }

    pub fn get(&self, file_key: &FileKey) -> TileData {
        let item = self
            .cache
            .get(file_key)
            .unwrap_or_else(|| panic!("Unable to find file: {}", file_key));

        let filepath = self.directory.as_path();
        let filepath = filepath.join(&item.filename);
        info!("TileFileCache: get {}", filepath.display());
        let f = OpenOptions::new().read(true).open(filepath);
        let f = match f {
            Ok(file) => file,
            Err(err) => panic!("Unable to open file for reading: {:?}", err),
        };
        serde_cbor::from_reader(f).expect("Unable to deserialize from file")
    }

    pub fn load(&mut self) {
        self.load_tiles_meta()
    }

    fn load_tiles_meta(&mut self) {
        info!("load from directory {}", self.directory.display());
        for entry in fs::read_dir(self.directory.as_path()).unwrap() {
            if let Ok(entry) = entry {
                let path = entry.path();
                if path.is_file() {
                    if let Some(filename) = entry.file_name().to_str() {
                        let ti = filename_to_tile_info(filename);
                        let meta = Meta {
                            filename: filename.to_owned(),
                            tile_info: ti,
                        };
                        let file_key = FileKey::from(&meta.tile_info);
                        self.cache.insert(file_key, meta);
                    }
                }
            }
        }
    }

    // not require load file
    pub fn get_tile_info(&self, file_key: &FileKey) -> &TileInfo {
        let item = self
            .cache
            .get(file_key)
            .unwrap_or_else(|| panic!("Unable to find file: {}", file_key));
        &item.tile_info
    }
    pub fn get_tile_info_mut(&mut self, file_key: &FileKey) -> &mut TileInfo {
        let item = self
            .cache
            .get_mut(file_key)
            .unwrap_or_else(|| panic!("Unable to find file: {}", file_key));
        &mut item.tile_info
    }
    pub fn update_tile_id(&mut self, file_key: &FileKey, id: TileId) -> &TileInfo {
        let mut tile_info = &mut self
            .cache
            .get_mut(file_key)
            .unwrap_or_else(|| panic!("Unable to find file: {}", file_key))
            .tile_info;
        tile_info.id = id;
        tile_info
    }
    pub fn file_keys(&self) -> Vec<FileKey> {
        self.cache.keys().cloned().collect()
    }
}

fn tile_info_to_filename(ti: &TileInfo) -> String {
    if ti.data_key.contains("-") {
        panic!("data_key doesn't support '-' symbol");
    }
    let r = &ti.border_rect;
    let rstr = format!(
        "{}-{}-{}-{}",
        HexSlice::new(&r.bl.x.value.to_be_bytes()),
        HexSlice::new(&r.bl.y.value.to_be_bytes()),
        HexSlice::new(&r.tr.x.value.to_be_bytes()),
        HexSlice::new(&r.tr.y.value.to_be_bytes())
    );
    format!(
        "{}-{}-{}-{}-{}-{}",
        ti.id.idx, ti.provider_id, ti.data_key, ti.revision, ti.zoom_level, rstr,
    )
}

fn filename_to_tile_info(filename: &str) -> TileInfo {
    let vec: Vec<&str> = filename.split("-").collect();
    let id: u64 = vec[0].parse().unwrap();
    let provider_id = vec[1].to_owned();
    let data_key = vec[2].to_owned();
    let revision: u32 = vec[3].parse().unwrap();
    let zoom_level: u32 = vec[4].parse().unwrap();
    let xls = hex::decode(vec[5]).unwrap();
    let ybs = hex::decode(vec[6]).unwrap();
    let xrs = hex::decode(vec[7]).unwrap();
    let yts = hex::decode(vec[8]).unwrap();
    let xl = f64::from_be_bytes(xls.try_into().unwrap());
    let yb = f64::from_be_bytes(ybs.try_into().unwrap());
    let xr = f64::from_be_bytes(xrs.try_into().unwrap());
    let yt = f64::from_be_bytes(yts.try_into().unwrap());

    TileInfo {
        id: TileId::new(id),
        revision: revision,
        provider_id: provider_id,
        data_key: data_key,
        zoom_level: zoom_level,
        border_rect: Rect::new(
            Vertex2::new(Meter::new(xl), Meter::new(yb)),
            Vertex2::new(Meter::new(xr), Meter::new(yt)),
        ),
    }
}

struct HexSlice<'a>(&'a [u8]);

impl<'a> HexSlice<'a> {
    fn new<T>(data: &'a T) -> HexSlice<'a>
    where
        T: ?Sized + AsRef<[u8]> + 'a,
    {
        HexSlice(data.as_ref())
    }
}

impl fmt::Display for HexSlice<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for byte in self.0 {
            write!(f, "{:02x}", byte)?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn tile_info_to_filename_sample() {
        let t = TileInfo {
            id: TileId::new(11),
            revision: 1,
            provider_id: "prov".to_owned(),
            data_key: "dk".to_owned(),
            zoom_level: 2,
            border_rect: Rect {
                bl: Vertex2::new(Meter::new(0.), Meter::new(1.)),
                tr: Vertex2::new(Meter::new(2.), Meter::new(3.)),
            },
        };
        assert_eq!(
            tile_info_to_filename(&t),
            "11-prov-dk-1-2-0000000000000000-3ff0000000000000-4000000000000000-4008000000000000"
                .to_owned()
        );
    }

    #[test]
    fn filename_to_tile_info_sample() {
        let t = filename_to_tile_info(
            "11-prov-dk-1-2-0000000000000000-3ff0000000000000-4000000000000000-4008000000000000",
        );

        let exp = TileInfo {
            id: TileId::new(11),
            revision: 1,
            provider_id: "prov".to_owned(),
            data_key: "dk".to_owned(),
            zoom_level: 2,
            border_rect: Rect {
                bl: Vertex2::new(Meter::new(0.), Meter::new(1.)),
                tr: Vertex2::new(Meter::new(2.), Meter::new(3.)),
            },
        };

        assert_eq!(t, exp);
    }
}
