use crate::tiledata::{TileData, TileId, TileInfo};
use crate::tilefilecache::{FileKey, TileFileCache};
use crate::util;
use rtree::common::Rect;
use rtree::rtree::RTree;
use std::collections::HashMap;
use std::path::Path;
use units::distance::Meter;

pub struct TileFileStorage {
    file_cache: TileFileCache,
    file_map: HashMap<TileId, FileKey>,
    file_rtree: RTree<f64, TileId, 8>,
}

impl TileFileStorage {
    pub fn new(directory: &Path) -> Self {
        TileFileStorage {
            file_cache: TileFileCache::new(directory),
            file_map: HashMap::new(),
            file_rtree: RTree::new(),
        }
    }

    pub fn load<F: FnMut(&TileInfo)>(&mut self, mut f: F) -> u64 {
        self.file_cache.load();
        let mut index = 0;
        for file_key in self.file_cache.file_keys() {
            let id = TileId::new(index);
            self.file_map.insert(id.clone(), file_key.clone());
            let mut ti = self.file_cache.get_tile_info_mut(&file_key);
            ti.id = id;
            let rect = util::convert_rect(&ti.border_rect);
            f(&ti);
            self.file_cache.update_tile_id(&file_key, id);
            self.file_rtree.insert(rect, id);
            index += 1;
        }
        index
    }

    pub fn put_file_storage(&mut self, id: TileId, rect: &Rect<Meter>, tile_data: &TileData) {
        let r = util::convert_rect(rect);
        let file_key = self.file_cache.put(tile_data);
        self.file_map.insert(id.clone(), file_key);
        self.file_rtree.insert(r, id);
    }

    pub fn remove_file_storage(&mut self, id: &TileId, rect: &Rect<Meter>) {
        let r = util::convert_rect(rect);
        let file_key_old = self
            .file_map
            .remove(id)
            .expect("TileFileStorage: Unable to remove file storage");
        self.file_cache.remove(&file_key_old);
        self.file_rtree.remove(r, id.clone());
    }

    pub fn find_by_rect(&self, rect: &Rect<Meter>) -> HashMap<TileId, TileInfo> {
        let rect = util::convert_rect(rect);
        let mut tile_map = HashMap::new();
        let _ = self.file_rtree.search(&rect, |id| {
            let file_key = self
                .file_map
                .get(&id)
                .expect("TileFileStorage: Unable to find id");
            let ti = self.file_cache.get_tile_info(&file_key);
            tile_map.insert(ti.id, ti.clone());
            true
        });

        tile_map
    }

    pub fn get(&self, id: &TileId) -> Option<TileData> {
        let file_key = self.file_map.get(id)?;
        Some(self.file_cache.get(&file_key))
    }
}
