use crate::memorypool::MemoryPool;
use crate::tiledata::{TileData, TileId, TileInfo};
use crate::tilefilestorage::TileFileStorage;
use crate::util;
use log::info;
use rtree::common::Rect;
use rtree::rtree::RTree;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::path::Path;
use std::time::Instant;
use units::distance::Meter;

struct TileMemoryInfo {
    idx: usize,
    time_check: Instant,
}

struct TileMemoryStorage {
    tile_memory_count_limit: usize,
    memory_pool: MemoryPool<TileData>,
    memory_map: HashMap<TileId, TileMemoryInfo>,
    memory_rtree: RTree<f64, usize, 8>,
}

impl TileMemoryStorage {
    fn new(tile_memory_count_limit: usize) -> Self {
        TileMemoryStorage {
            tile_memory_count_limit: tile_memory_count_limit,
            memory_pool: MemoryPool::new(),
            memory_map: HashMap::new(),
            memory_rtree: RTree::new(),
        }
    }

    fn try_remove_memory_cache(&mut self, id: &TileId) -> bool {
        match self.memory_map.remove(id) {
            Some(d) => {
                let idx = usize::try_from(d.idx).unwrap();
                let rect = self.memory_pool.data(idx).tile_info.border_rect;
                self.memory_pool.release(idx);
                self.memory_rtree.remove(util::convert_rect(&rect), d.idx);
                true
            }
            None => false,
        }
    }

    fn remove_memory_cache(&mut self, id: &TileId) {
        if !self.try_remove_memory_cache(id) {
            panic!("TileStorage: Unable to remove tile from memory cache");
        }
    }

    fn put_memory_cache(
        &mut self,
        id: TileId,
        rect: &Rect<Meter>,
        tile_data: TileData,
    ) -> &TileData {
        if self.tile_memory_count_limit > 0 && self.memory_map.len() >= self.tile_memory_count_limit
        {
            // remove oldest
            let oldest = self.find_oldest();
            if let Some(oid) = oldest {
                info!("TileStorage: remove from memory cache: {}", oid);
                let n = *oid;
                self.remove_memory_cache(&n);
            }
        }

        let idx = self.memory_pool.put(tile_data);
        self.memory_map.insert(
            id,
            TileMemoryInfo {
                idx: idx,
                time_check: Instant::now(),
            },
        );
        self.memory_rtree.insert(util::convert_rect(rect), idx);
        self.memory_pool.data(idx)
    }

    fn find_by_rect(&self, rect: &Rect<Meter>) -> HashMap<TileId, TileInfo> {
        let rect = util::convert_rect(rect);
        let mut tile_map = HashMap::new();
        let _ = self.memory_rtree.search(&rect, |&idx| {
            let ti = &self.memory_pool.data(idx).tile_info;
            let id = ti.id;
            tile_map.insert(id, ti.clone());
            true
        });

        tile_map
    }

    fn get(&self, id: &TileId) -> Option<&TileData> {
        let info = self.memory_map.get(id)?;
        Some(self.memory_pool.data(info.idx))
    }

    fn find_oldest(&self) -> Option<&TileId> {
        let mut ret: Option<&TileId> = None;
        let mut time: Option<Instant> = None;
        for (k, v) in self.memory_map.iter() {
            match time {
                Some(t) => {
                    if t > v.time_check {
                        time = Some(v.time_check);
                        ret = Some(k);
                    }
                }
                None => {
                    ret = Some(k);
                    time = Some(v.time_check);
                }
            }
        }
        ret
    }
}

struct StorageData {
    // autoincrement runtime index, represent tile id
    index: u64,
    // dictionary to represent relation provider id, data key to tile id
    tile_dict: HashMap<ProviderIdDataKey, TileId>,
    // memory cache
    memory_storage: TileMemoryStorage,
    file_storage: TileFileStorage,
}

impl StorageData {
    fn new(directory: &Path) -> StorageData {
        let mut d = StorageData {
            index: 0,
            tile_dict: HashMap::new(),
            memory_storage: TileMemoryStorage::new(10),
            file_storage: TileFileStorage::new(directory),
        };
        let mut tile_dict = HashMap::new();
        let count = d.file_storage.load(|v| {
            tile_dict.insert(
                ProviderIdDataKey::new(v.provider_id.clone(), v.data_key.clone()),
                v.id,
            );
        });
        d.tile_dict = tile_dict;
        d.index = count;
        d
    }
}

pub struct TileStorage {
    data: StorageData,
}

impl TileStorage {
    pub fn new(directory: &Path) -> TileStorage {
        TileStorage {
            data: StorageData::new(directory),
        }
    }

    pub fn put(&mut self, mut tile_data: TileData) -> TileId {
        let rect = tile_data.tile_info.border_rect.clone();
        let key = ProviderIdDataKey::new(
            tile_data.tile_info.provider_id.clone(),
            tile_data.tile_info.data_key.clone(),
        );

        match self.data.tile_dict.get(&key) {
            Some(id) => {
                let id = *id;
                tile_data.tile_info.id = id;
                self.data.file_storage.remove_file_storage(&id, &rect);
                self.data
                    .file_storage
                    .put_file_storage(id, &rect, &tile_data);
                self.data.memory_storage.try_remove_memory_cache(&id);
                self.data
                    .memory_storage
                    .put_memory_cache(id, &rect, tile_data);
                id
            }
            None => {
                let id = TileId::new(self.data.index);
                tile_data.tile_info.id = id;
                self.data.tile_dict.insert(key, id);
                self.data
                    .file_storage
                    .put_file_storage(id, &rect, &tile_data);
                self.data
                    .memory_storage
                    .put_memory_cache(id, &rect, tile_data);
                self.data.index += 1;
                id
            }
        }
    }

    pub fn find(&self, rect: &Rect<Meter>) -> Vec<TileInfo> {
        let mut a = self.data.memory_storage.find_by_rect(&rect);
        let b = self.data.file_storage.find_by_rect(&rect);
        a.extend(b);

        a.into_values().collect()
    }

    pub fn get_data(&mut self, id: TileId) -> TileData {
        // first search in memory cache
        // then in file storage
        match self.data.memory_storage.get(&id) {
            Some(ret) => ret.clone(),
            None => {
                let td: TileData = self
                    .data
                    .file_storage
                    .get(&id)
                    .expect("TileStorage: tile index miss");
                let rect = td.tile_info.border_rect.clone();
                self.data
                    .memory_storage
                    .put_memory_cache(id, &rect, td)
                    .clone()
            }
        }
    }
}

#[derive(PartialEq, Eq, Hash)]
struct ProviderIdDataKey {
    provider_id: String,
    data_key: String,
}

impl ProviderIdDataKey {
    fn new(provider_id: String, data_key: String) -> ProviderIdDataKey {
        ProviderIdDataKey {
            provider_id: provider_id,
            data_key: data_key,
        }
    }
}
