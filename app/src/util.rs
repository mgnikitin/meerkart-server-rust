use rtree::common::{Rect, Vertex2};
use units::distance::Meter;

pub fn convert_rect(r: &Rect<Meter>) -> Rect<f64> {
    Rect::new(
        Vertex2::new(r.bl.x.value, r.bl.y.value),
        Vertex2::new(r.tr.x.value, r.tr.y.value),
    )
}
