use protobuf_codegen_pure::Customize;

fn main() {
    // protobuf_codegen_pure::run(protobuf_codegen_pure::Args {
    //     out_dir: "src",
    //     input: &["src/proto/tiledata.proto"],
    //     includes: &["src"],
    //     customize: protobuf_codegen_pure::Customize {
    //         ..Default::default()
    //     },
    // })
    // .expect("protoc");

    protobuf_codegen_pure::Codegen::new()
        .customize(Customize {
            serde_derive: Some(true),
            //    gen_mod_rs: Some(true),
            ..Default::default()
        })
        .out_dir("src")
        .input("src/proto/tiledata.proto")
        .include("src")
        .run()
        .expect("Codegen failed.");
}
