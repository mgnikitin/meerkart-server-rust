use serde::{Deserialize, Serialize};
use std::cmp::PartialOrd;
use std::marker::Copy;
use std::ops::{Add, Mul, Sub};

#[derive(Default, Copy, Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Vertex2<T>
where
    T: Copy + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + PartialOrd + Default,
{
    pub x: T,
    pub y: T,
}

impl<T> Vertex2<T>
where
    T: Copy + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + PartialOrd + Default,
{
    pub fn new(x: T, y: T) -> Vertex2<T> {
        Vertex2 { x: x, y: y }
    }
}

pub fn min<T>(a: &Vertex2<T>, b: &Vertex2<T>) -> Vertex2<T>
where
    T: Copy + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + PartialOrd + Default,
{
    let x = if a.x < b.x { a.x } else { b.x };
    let y = if a.y < b.y { a.y } else { b.y };
    Vertex2::new(x, y)
}

pub fn max<T>(a: &Vertex2<T>, b: &Vertex2<T>) -> Vertex2<T>
where
    T: Copy + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + PartialOrd + Default,
{
    let x = if a.x > b.x { a.x } else { b.x };
    let y = if a.y > b.y { a.y } else { b.y };
    Vertex2::new(x, y)
}

#[derive(Default, Copy, Clone, PartialEq, Serialize, Deserialize, Debug)]
pub struct Rect<T>
where
    T: Copy + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + PartialOrd + Default,
{
    pub bl: Vertex2<T>,
    pub tr: Vertex2<T>,
}

impl<T> Rect<T>
where
    T: Copy + Sub<Output = T> + Mul<Output = T> + Add<Output = T> + PartialOrd + Default,
{
    pub fn new(bl: Vertex2<T>, tr: Vertex2<T>) -> Rect<T> {
        Rect { bl: bl, tr: tr }
    }

    pub fn width(&self) -> T {
        self.tr.x - self.bl.x
    }

    pub fn height(&self) -> T {
        self.tr.y - self.bl.y
    }

    pub fn contains(&self, v: Vertex2<T>) -> bool {
        !(v.x < self.bl.x || v.x > self.tr.x || v.y < self.bl.y || v.y > self.tr.y)
    }

    /// \brief Проверка на пересечение или попадание прямоугольных областей
    pub fn intersects(&self, r: Rect<T>) -> bool {
        !(r.bl.x >= self.tr.x || r.tr.x <= self.bl.x || r.bl.y >= self.tr.y || r.tr.y <= self.bl.y)
    }
    /// \brief Проверка на касание, пересечение или попадание прямоугольных областей. Возвращает true если участки касаются, пересекаются или один из участков содержит другой.
    pub fn touches(&self, r: &Rect<T>) -> bool {
        !(r.bl.x > self.tr.x || r.tr.x < self.bl.x || r.bl.y > self.tr.y || r.tr.y < self.bl.y)
    }

    pub fn near(&self, mut v: Vertex2<T>) -> Vertex2<T> {
        if v.x < self.bl.x {
            v.x = self.bl.x;
        } else if v.x > self.tr.x {
            v.x = self.tr.x;
        }
        if v.y < self.bl.y {
            v.y = self.bl.y;
        } else if v.y > self.tr.y {
            v.y = self.tr.y;
        }
        v
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn width() {
        let bl = Vertex2::new(10, 10);
        let tr = Vertex2::new(20, 30);
        let r = Rect::new(bl, tr);

        assert_eq!(r.width(), 10);
    }

    #[test]
    fn height() {
        let bl = Vertex2::new(10, 10);
        let tr = Vertex2::new(20, 30);
        let r = Rect::new(bl, tr);

        assert_eq!(r.height(), 20);
    }
}
