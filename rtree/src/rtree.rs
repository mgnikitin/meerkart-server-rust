extern crate array_init;
extern crate num_traits;

use crate::common::*;
use ::array_init::array_init;
use num_traits::{Float, Zero};

trait RTreeRect<T>
where
    T: Float + Default,
{
    fn volume(&self) -> T;

    fn combined(&self, rect: &Rect<T>) -> Rect<T>;
}

impl<T> RTreeRect<T> for Rect<T>
where
    T: Float + Default,
{
    fn volume(&self) -> T {
        self.width() * self.height()
    }

    fn combined(&self, rect: &Rect<T>) -> Rect<T> {
        let bl = min(&self.bl, &rect.bl);
        let tr = max(&self.tr, &rect.tr);
        Rect::new(bl, tr)
    }
}

#[derive(Default, Clone)]
struct RTreeBranch<T, DATATYPE, const MAXNODES: usize>
where
    T: Float + Default,
    DATATYPE: Copy + Eq,
{
    pub rect: Rect<T>,
    pub data: Option<DATATYPE>,
    node: Option<Box<RTreeNode<T, DATATYPE, MAXNODES>>>,
}

impl<T, DATATYPE, const MAXNODES: usize> RTreeBranch<T, DATATYPE, MAXNODES>
where
    T: Float + Default,
    DATATYPE: Copy + Eq,
{
    // fn new() -> Self {
    //     let r = Default::default();
    //     RTreeBranch {
    //         rect: r,
    //         data: None,
    //         node: None,
    //     }
    // }

    fn new(
        rect: Rect<T>,
        data: Option<DATATYPE>,
        node: Option<Box<RTreeNode<T, DATATYPE, MAXNODES>>>,
    ) -> Self {
        RTreeBranch {
            rect: rect,
            data: data,
            node: node,
        }
    }
}

struct RTreeGroup<T, const MAXNODES: usize>
where
    T: Float + Default,
{
    pub indexes: Vec<usize>,
    pub cover: Rect<T>,
    //pub count: usize,
}

impl<T, const MAXNODES: usize> RTreeGroup<T, MAXNODES>
where
    T: Float + Default,
{
    fn new() -> Self {
        RTreeGroup {
            indexes: Vec::new(),
            cover: Default::default(),
            // count: 0,
        }
    }

    pub fn classify(&mut self, rect: &Rect<T>, index: usize) {
        if self.indexes.is_empty() {
            self.cover = rect.clone();
        } else {
            self.cover = self.cover.combined(rect);
        }
        self.indexes.push(index);
    }

    pub fn count(&self) -> usize {
        self.indexes.len()
    }
}

// const MAXNODES: usize = 8;

fn pick_seed<T: Float + Default, DATATYPE: Copy + Eq, const MAXNODES: usize>(
    area_branches: &[(T, RTreeBranch<T, DATATYPE, MAXNODES>)],
) -> (usize, usize) {
    let mut seed0 = 0;
    let mut seed1 = 1;
    let mut worst = area_branches[0]
        .1
        .rect
        .combined(&area_branches[1].1.rect)
        .volume()
        - area_branches[0].0
        - area_branches[1].0;

    for (i, x) in area_branches.iter().enumerate() {
        for (j, y) in area_branches[1..].iter().enumerate() {
            let r = x.1.rect.combined(&y.1.rect);
            let incr = r.volume() - x.0 - y.0;
            if incr > worst {
                worst = incr;
                seed0 = i;
                seed1 = j;
            }
        }
    }

    (seed0, seed1)
}

fn insert_rec<T: Float + Default, DATATYPE: Copy + Eq, const MAXNODES: usize>(
    branch: RTreeBranch<T, DATATYPE, MAXNODES>,
    parent: &mut RTreeNode<T, DATATYPE, MAXNODES>,
    level: u32,
) -> Option<Box<RTreeNode<T, DATATYPE, MAXNODES>>> {
    if parent.level > level {
        // go futher
        let idx = parent.optimal_branch(&branch.rect);
        let parent_branch = &mut parent.branch[idx];
        match &mut parent_branch.node {
            Some(p) => {
                let branch_rect = branch.rect.clone();
                let new_node = insert_rec(branch, p, level);
                match new_node {
                    Some(n) => {
                        // Child was split
                        parent_branch.rect = p.node_cover();
                        let node_cover = n.node_cover();
                        let new_branch = RTreeBranch::new(node_cover, None, Some(n));
                        if parent.count < MAXNODES {
                            parent.add_branch(new_branch);
                            return None;
                        }
                        return Some(split(new_branch, parent));
                    }
                    None => {
                        parent_branch.rect = parent_branch.rect.combined(&branch_rect);
                        return None;
                    }
                }
            }
            None => panic!("Unable to get parent branch node"),
        }
    } else {
        // leaf
        if parent.count < MAXNODES {
            parent.add_branch(branch);
            return None;
        }
        return Some(split(branch, parent));
    }
}

fn remove_rec<T: Float + Default, DATATYPE: Copy + Eq, const MAXNODES: usize>(
    branch: &RTreeBranch<T, DATATYPE, MAXNODES>,
    parent: &mut RTreeNode<T, DATATYPE, MAXNODES>,
    reinsert_vec: &mut Vec<RTreeNode<T, DATATYPE, MAXNODES>>,
) -> bool {
    const MINNODES: usize = 4;
    if parent.is_internal_node() {
        // go futher
        for index in 0..parent.count {
            let parent_branch = &mut parent.branch[index];
            if branch.rect.touches(&parent_branch.rect) {
                match &mut parent_branch.node {
                    Some(p) => {
                        if !remove_rec(branch, &mut *p, reinsert_vec) {
                            if p.count >= MINNODES {
                                parent_branch.rect = p.node_cover();
                            } else {
                                let node: &mut RTreeNode<T, DATATYPE, MAXNODES> = p;
                                reinsert_vec.push(node.clone());
                                parent.remove_branch(index);
                            }
                            return false;
                        }
                    }
                    None => panic!("Unable to get parent branch node"),
                }
            }
        }
    } else {
        // leaf
        for index in 0..parent.count {
            if parent.branch[index].data == branch.data {
                parent.remove_branch(index);
                return false;
            }
        }
    }

    return true;
}

fn search_rec<
    T: Float + Default,
    DATATYPE: Copy + Eq,
    F: FnMut(&DATATYPE) -> bool,
    const MAXNODES: usize,
>(
    parent: &RTreeNode<T, DATATYPE, MAXNODES>,
    rect: &Rect<T>,
    f: &mut F,
) -> (bool, usize) {
    let mut found_count: usize = 0;

    if parent.is_internal_node() {
        for index in 0..parent.count {
            let parent_branch = &parent.branch[index];
            if rect.touches(&parent_branch.rect) {
                match &parent_branch.node {
                    Some(node) => {
                        let (ret, count) = search_rec(&node, &rect, f);
                        found_count += count;
                        if !ret {
                            return (false, found_count); // Don't continue searching
                        }
                    }
                    None => panic!("Unable to get parent branch node"),
                }
            }
        }
    } else {
        // leaf

        assert!(parent.is_leaf());
        for index in 0..parent.count {
            let parent_branch = &parent.branch[index];
            if rect.touches(&parent_branch.rect) {
                match parent_branch.data {
                    Some(id) => {
                        found_count += 1;
                        if !f(&id) {
                            return (false, found_count); // Don't continue searching
                        }
                    }
                    None => {
                        assert!(false);
                        continue;
                    }
                }
            }
        }
    }

    (true, found_count) // Continue searching
}

fn split<T: Float + Default, DATATYPE: Copy + Eq, const MAXNODES: usize>(
    branch: RTreeBranch<T, DATATYPE, MAXNODES>,
    parent: &mut RTreeNode<T, DATATYPE, MAXNODES>,
) -> Box<RTreeNode<T, DATATYPE, MAXNODES>> {
    let mut node = RTreeNode::new();
    node.level = parent.level;

    let prev = parent.clone();
    *parent = prev.partition(branch, &mut node);
    Box::new(node)
}

#[derive(Clone)]
struct RTreeNode<T, DATATYPE, const MAXNODES: usize>
where
    T: Float + Default,
    DATATYPE: Copy + Eq,
{
    pub branch: [RTreeBranch<T, DATATYPE, MAXNODES>; MAXNODES],
    pub count: usize,
    pub level: u32,
}

impl<T, DATATYPE, const MAXNODES: usize> RTreeNode<T, DATATYPE, MAXNODES>
where
    T: Float + Zero + Default,
    DATATYPE: Copy + Eq,
{
    fn new() -> RTreeNode<T, DATATYPE, MAXNODES> {
        RTreeNode {
            branch: array_init(|_| RTreeBranch::new(Default::default(), None, None)),
            count: 0,
            level: 0,
        }
    }

    // Not a leaf, but a internal node
    pub fn is_internal_node(&self) -> bool {
        self.level > 0
    }

    // A leaf, contains data
    pub fn is_leaf(&self) -> bool {
        return self.level == 0;
    }

    pub fn optimal_branch(&self, rect: &Rect<T>) -> usize {
        let irect0 = &self.branch[0].rect;
        let mut resarea = irect0.volume();
        let crect0 = rect.combined(irect0);
        let mut resincr = crect0.volume() - resarea;
        let mut resindex: usize = 0;

        for i in 1..self.count {
            let irect = &self.branch[i].rect;
            let area = irect.volume();
            let crect = rect.combined(irect);
            let increase = crect.volume() - area;

            if resincr > increase {
                resincr = increase;
                resarea = area;
                resindex = i;
            } else if (resincr == increase) && (area < resarea) {
                resincr = increase;
                resarea = area;
                resindex = i;
            }
        }

        resindex
    }

    pub fn add_branch(&mut self, branch: RTreeBranch<T, DATATYPE, MAXNODES>) {
        self.branch[self.count] = branch;
        self.count += 1;
    }

    pub fn remove_branch(&mut self, index: usize) {
        assert!(self.count > 0);

        if 1 == self.count {
            self.branch[index].node = None;
        } else {
            self.branch.swap(index, self.count - 1);
        }
        self.count -= 1;
    }

    pub fn partition(
        self,
        branch: RTreeBranch<T, DATATYPE, MAXNODES>,
        node: &mut RTreeNode<T, DATATYPE, MAXNODES>,
    ) -> RTreeNode<T, DATATYPE, MAXNODES> {
        assert!(self.count == MAXNODES);
        let mut area_branches: Vec<(T, RTreeBranch<T, DATATYPE, MAXNODES>)> = Vec::new();
        let mut group0 = RTreeGroup::new();
        let mut group1 = RTreeGroup::new();
        let total: usize = MAXNODES + 1;
        // init
        // let branches = Vec::new(); //[MAXNODES + 1];
        for item in self.branch {
            area_branches.push((item.rect.volume(), item));
        }
        area_branches.push((branch.rect.volume(), branch));
        let mut last = area_branches.len() - 1;
        let (seed0, seed1) = pick_seed(&area_branches);
        area_branches.swap(seed0, last);
        group0.classify(&area_branches.get(last).unwrap().1.rect, last);
        last -= 1;
        area_branches.swap(seed1, last);
        group1.classify(&area_branches.get(last).unwrap().1.rect, last);
        last -= 1;

        let get_group = |group0: &RTreeGroup<T, MAXNODES>,
                         group1: &RTreeGroup<T, MAXNODES>,
                         area_branches: &Vec<(T, RTreeBranch<T, DATATYPE, MAXNODES>)>,
                         last|
         -> (usize, bool) {
            let mut index_ret = 0;
            let mut flag_0 = true;
            let mut biggest_diff: Option<T> = None;
            for index in 0..=last {
                let cur_rect = &area_branches.get(index).unwrap().1.rect;
                let rect0 = cur_rect.combined(&group0.cover);
                let rect1 = cur_rect.combined(&group1.cover);
                let growth0 = rect0.volume() - group0.cover.volume();
                let growth1 = rect1.volume() - group1.cover.volume();
                let mut diff = growth1 - growth0;
                let (flag, group) = if diff >= Zero::zero() {
                    (true, &group0)
                } else {
                    (false, &group1)
                };
                if diff < Zero::zero() {
                    diff = -diff;
                }
                match biggest_diff {
                    Some(biggest) => {
                        let group_ret = if flag_0 { &group0 } else { &group1 };
                        if diff > biggest {
                            biggest_diff = Some(diff);
                            index_ret = index;
                            flag_0 = flag;
                        } else if (diff == biggest) && (group.count() < group_ret.count()) {
                            index_ret = index;
                            flag_0 = flag;
                        }
                    }
                    None => biggest_diff = Some(diff),
                }
            }
            (index_ret, flag_0)
        };

        const MINNODES: usize = 4;
        while (group0.count() < (total - MINNODES)) && (group1.count() < (total - MINNODES)) {
            let p = get_group(&group0, &group1, &area_branches, last.clone());
            area_branches.swap(p.0, last);
            let group = if p.1 { &mut group0 } else { &mut group1 };
            group.classify(&area_branches.get(last).unwrap().1.rect, last);
            if 0 == last {
                break;
            }
            last -= 1;
        }
        // If one group too full, put remaining rects in the other
        if (group0.count() + group1.count()) < total {
            assert!(last < total);
            let group = if group0.count() >= (total - MINNODES) {
                &mut group1
            } else {
                &mut group0
            };
            for index in 0..=last {
                group.classify(&area_branches.get(index).unwrap().1.rect, index);
            }
        }
        assert!((group0.count() + group1.count()) == total);
        assert!((group0.count() >= MINNODES) && (group1.count() >= MINNODES));

        let fill_node = |group: RTreeGroup<T, MAXNODES>,
                         node: &mut RTreeNode<T, DATATYPE, MAXNODES>| {
            node.count = 0;
            for idx in group.indexes {
                node.add_branch(area_branches[idx].1.clone());
            }
        };
        fill_node(group1, node);
        let mut node1 = RTreeNode::new();
        node1.level = self.level;
        fill_node(group0, &mut node1);
        node1
    }

    pub fn node_cover(&self) -> Rect<T> {
        if self.count == 0 {
            assert!(false);
            return Default::default();
        }
        let mut rect = self.branch[0].rect.clone();
        for idx in 1..self.count {
            rect = rect.combined(&self.branch[idx].rect);
        }
        rect
    }
}

pub struct RTree<T, DATATYPE, const MAXNODES: usize>
where
    T: Float + Default,
    DATATYPE: Copy + Eq,
{
    root: Box<RTreeNode<T, DATATYPE, MAXNODES>>,
}

impl<T, DATATYPE, const MAXNODES: usize> RTree<T, DATATYPE, MAXNODES>
where
    T: Float + Default,
    DATATYPE: Copy + Eq,
{
    pub fn new() -> RTree<T, DATATYPE, MAXNODES> {
        RTree {
            root: Box::new(RTreeNode::new()),
        }
    }

    pub fn insert(&mut self, rect: Rect<T>, data: DATATYPE) {
        self.insert_level(rect, data, 0)
    }

    pub fn remove(&mut self, rect: Rect<T>, data: DATATYPE) {
        let mut reinsert_vec = Vec::new();
        let br = RTreeBranch::new(rect, Some(data), None);
        if !remove_rec(&br, &mut self.root, &mut reinsert_vec) {
            for node in reinsert_vec {
                for index in 0..node.count {
                    let branch = &node.branch[index];
                    self.insert_level(branch.rect.clone(), branch.data.unwrap(), node.level);
                }
            }

            // Check for redundant root (not leaf, 1 child) and eliminate
            if self.root.count == 1 && self.root.is_internal_node() {
                match &mut self.root.branch[0].node {
                    Some(p) => {
                        let mut tmp = p.clone();
                        std::mem::swap(&mut tmp, &mut self.root);
                    }
                    None => panic!("Unable to get root branch node"),
                }
            }
        }
    }

    pub fn search<F: FnMut(&DATATYPE) -> bool>(&self, rect: &Rect<T>, mut callback: F) -> usize {
        let (_, ret) = search_rec(&self.root, rect, &mut callback);
        ret
    }

    fn insert_level(&mut self, rect: Rect<T>, data: DATATYPE, level: u32) {
        let branch = RTreeBranch {
            rect: rect,
            data: Some(data),
            node: None,
        };
        let node_opt = insert_rec(branch, &mut self.root, level);
        match node_opt {
            Some(node) => {
                let mut new_root = Box::new(RTreeNode::new());
                new_root.level = self.root.level + 1;
                let root_cover = self.root.node_cover();
                let node_cover = node.node_cover();
                std::mem::swap(&mut self.root, &mut new_root);
                self.root.add_branch(RTreeBranch {
                    rect: root_cover,
                    data: None,
                    node: Some(new_root),
                });
                self.root.add_branch(RTreeBranch {
                    rect: node_cover,
                    data: None,
                    node: Some(node),
                });
            }
            None => {}
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn optimal_branch() {
        let mut node: RTreeNode<f64, usize, 8> = RTreeNode::new();
        node.add_branch(RTreeBranch::new(
            Rect::new(Vertex2::new(0., 0.), Vertex2::new(10., 10.)),
            None,
            None,
        ));
        node.add_branch(RTreeBranch::new(
            Rect::new(Vertex2::new(10., 0.), Vertex2::new(20., 10.)),
            None,
            None,
        ));
        let r = Rect::new(Vertex2::new(10., 10.), Vertex2::new(20., 20.));

        assert_eq!(node.optimal_branch(&r), 1);
    }
}
