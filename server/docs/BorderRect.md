# BorderRect

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bottom_left** | [***models::Vertex2**](Vertex2.md) |  | [optional] [default to None]
**top_right** | [***models::Vertex2**](Vertex2.md) |  | [optional] [default to None]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


