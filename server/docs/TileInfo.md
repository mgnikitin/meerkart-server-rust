# TileInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [***models::TileId**](TileId.md) |  | [optional] [default to None]
**revision** | **usize** |  | [optional] [default to None]
**provider_id** | **String** |  | [optional] [default to None]
**data_key** | **String** |  | [optional] [default to None]
**zoom_level** | **usize** |  | [optional] [default to None]
**border_rect** | [***models::BorderRect**](BorderRect.md) |  | [optional] [default to None]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


