# default_api

All URIs are relative to *http://api.tilestore.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
**addTile**](default_api.md#addTile) | **POST** /tiles | Add tile data
**deleteTile**](default_api.md#deleteTile) | **DELETE** /tile/{id} | Delete a tile
**getTileDataById**](default_api.md#getTileDataById) | **GET** /tile/{id} | Returns a tile data
**getTilesByRect**](default_api.md#getTilesByRect) | **GET** /tiles/findByRect | Returns a list of tiles
**getVersion**](default_api.md#getVersion) | **GET** /version | Get API version


# **addTile**
> models::TileId addTile(optional)
Add tile data

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tile_data** | **swagger::ByteArray**|  | 

### Return type

[**models::TileId**](TileId.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTile**
> deleteTile(id)
Delete a tile

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **i64**| Tile id to delete | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTileDataById**
> swagger::ByteArray getTileDataById(id)
Returns a tile data

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **id** | **i64**| The tile ID | 

### Return type

[**swagger::ByteArray**](file.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTilesByRect**
> Vec<models::TileInfo> getTilesByRect(left, right, bottom, top, optional)
Returns a list of tiles

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
  **left** | **i64**| The left value | 
  **right** | **i64**| The right value | 
  **bottom** | **i64**| The bottom value | 
  **top** | **i64**| The top value | 
 **optional** | **map[string]interface{}** | optional parameters | nil if no parameters

### Optional Parameters
Optional parameters are passed through a map[string]interface{}.

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **left** | **i64**| The left value | 
 **right** | **i64**| The right value | 
 **bottom** | **i64**| The bottom value | 
 **top** | **i64**| The top value | 
 **zoom_level** | **i32**|  | [default to 0]

### Return type

[**Vec<models::TileInfo>**](TileInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getVersion**
> models::InlineResponse200 getVersion()
Get API version

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**models::InlineResponse200**](inline_response_200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

