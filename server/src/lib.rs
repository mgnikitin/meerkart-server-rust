#![allow(missing_docs, trivial_casts, unused_variables, unused_mut, unused_imports, unused_extern_crates, non_camel_case_types)]

use async_trait::async_trait;
use futures::Stream;
use std::error::Error;
use std::task::{Poll, Context};
use swagger::{ApiError, ContextWrapper};
use serde::{Serialize, Deserialize};

type ServiceError = Box<dyn Error + Send + Sync + 'static>;

pub const BASE_PATH: &'static str = "/v1";
pub const API_VERSION: &'static str = "0.0.1";

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum AddTileResponse {
    /// Added
    Added
    (models::TileId)
    ,
    /// Invalid input
    InvalidInput
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum DeleteTileResponse {
    /// Tile not found
    TileNotFound
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetTileDataByIdResponse {
    /// A binary object of tile
    ABinaryObjectOfTile
    (swagger::ByteArray)
    ,
    /// Tile not found
    TileNotFound
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
#[must_use]
pub enum GetTilesByRectResponse {
    /// A JSON array of tiles with info
    AJSONArrayOfTilesWithInfo
    (Vec<models::TileInfo>)
    ,
    /// Invalid parameter values
    InvalidParameterValues
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub enum GetVersionResponse {
    /// Object with current version string
    ObjectWithCurrentVersionString
    (models::InlineResponse200)
}

/// API
#[async_trait]
pub trait Api<C: Send + Sync> {
    fn poll_ready(&self, _cx: &mut Context) -> Poll<Result<(), Box<dyn Error + Send + Sync + 'static>>> {
        Poll::Ready(Ok(()))
    }

    /// Add tile data
    async fn add_tile(
        &self,
        tile_data: Option<swagger::ByteArray>,
        context: &C) -> Result<AddTileResponse, ApiError>;

    /// Delete a tile
    async fn delete_tile(
        &self,
        id: i64,
        context: &C) -> Result<DeleteTileResponse, ApiError>;

    /// Returns a tile data
    async fn get_tile_data_by_id(
        &self,
        id: i64,
        context: &C) -> Result<GetTileDataByIdResponse, ApiError>;

    /// Returns a list of tiles
    async fn get_tiles_by_rect(
        &self,
        left: i64,
        right: i64,
        bottom: i64,
        top: i64,
        zoom_level: Option<i32>,
        context: &C) -> Result<GetTilesByRectResponse, ApiError>;

    /// Get API version
    async fn get_version(
        &self,
        context: &C) -> Result<GetVersionResponse, ApiError>;

}

/// API where `Context` isn't passed on every API call
#[async_trait]
pub trait ApiNoContext<C: Send + Sync> {

    fn poll_ready(&self, _cx: &mut Context) -> Poll<Result<(), Box<dyn Error + Send + Sync + 'static>>>;

    fn context(&self) -> &C;

    /// Add tile data
    async fn add_tile(
        &self,
        tile_data: Option<swagger::ByteArray>,
        ) -> Result<AddTileResponse, ApiError>;

    /// Delete a tile
    async fn delete_tile(
        &self,
        id: i64,
        ) -> Result<DeleteTileResponse, ApiError>;

    /// Returns a tile data
    async fn get_tile_data_by_id(
        &self,
        id: i64,
        ) -> Result<GetTileDataByIdResponse, ApiError>;

    /// Returns a list of tiles
    async fn get_tiles_by_rect(
        &self,
        left: i64,
        right: i64,
        bottom: i64,
        top: i64,
        zoom_level: Option<i32>,
        ) -> Result<GetTilesByRectResponse, ApiError>;

    /// Get API version
    async fn get_version(
        &self,
        ) -> Result<GetVersionResponse, ApiError>;

}

/// Trait to extend an API to make it easy to bind it to a context.
pub trait ContextWrapperExt<C: Send + Sync> where Self: Sized
{
    /// Binds this API to a context.
    fn with_context(self: Self, context: C) -> ContextWrapper<Self, C>;
}

impl<T: Api<C> + Send + Sync, C: Clone + Send + Sync> ContextWrapperExt<C> for T {
    fn with_context(self: T, context: C) -> ContextWrapper<T, C> {
         ContextWrapper::<T, C>::new(self, context)
    }
}

#[async_trait]
impl<T: Api<C> + Send + Sync, C: Clone + Send + Sync> ApiNoContext<C> for ContextWrapper<T, C> {
    fn poll_ready(&self, cx: &mut Context) -> Poll<Result<(), ServiceError>> {
        self.api().poll_ready(cx)
    }

    fn context(&self) -> &C {
        ContextWrapper::context(self)
    }

    /// Add tile data
    async fn add_tile(
        &self,
        tile_data: Option<swagger::ByteArray>,
        ) -> Result<AddTileResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().add_tile(tile_data, &context).await
    }

    /// Delete a tile
    async fn delete_tile(
        &self,
        id: i64,
        ) -> Result<DeleteTileResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().delete_tile(id, &context).await
    }

    /// Returns a tile data
    async fn get_tile_data_by_id(
        &self,
        id: i64,
        ) -> Result<GetTileDataByIdResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().get_tile_data_by_id(id, &context).await
    }

    /// Returns a list of tiles
    async fn get_tiles_by_rect(
        &self,
        left: i64,
        right: i64,
        bottom: i64,
        top: i64,
        zoom_level: Option<i32>,
        ) -> Result<GetTilesByRectResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().get_tiles_by_rect(left, right, bottom, top, zoom_level, &context).await
    }

    /// Get API version
    async fn get_version(
        &self,
        ) -> Result<GetVersionResponse, ApiError>
    {
        let context = self.context().clone();
        self.api().get_version(&context).await
    }

}


#[cfg(feature = "client")]
pub mod client;

// Re-export Client as a top-level name
#[cfg(feature = "client")]
pub use client::Client;

#[cfg(feature = "server")]
pub mod server;

// Re-export router() as a top-level name
#[cfg(feature = "server")]
pub use self::server::Service;

#[cfg(feature = "server")]
pub mod context;

pub mod models;

#[cfg(any(feature = "client", feature = "server"))]
pub(crate) mod header;
