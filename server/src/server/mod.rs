use futures::{future, future::BoxFuture, future::FutureExt, stream, stream::TryStreamExt, Stream};
use hyper::header::{HeaderName, HeaderValue, CONTENT_TYPE};
use hyper::{Body, HeaderMap, Request, Response, StatusCode};
use log::warn;
use multipart::server::save::SaveResult;
use multipart::server::Multipart;
#[allow(unused_imports)]
use std::convert::{TryFrom, TryInto};
use std::error::Error;
use std::future::Future;
use std::marker::PhantomData;
use std::task::{Context, Poll};
pub use swagger::auth::Authorization;
use swagger::auth::Scopes;
use swagger::{ApiError, BodyExt, Has, RequestParser, XSpanIdString};
use url::form_urlencoded;

use crate::header;
#[allow(unused_imports)]
use crate::models;

pub use crate::context;

type ServiceFuture = BoxFuture<'static, Result<Response<Body>, crate::ServiceError>>;

use crate::{
    AddTileResponse, Api, DeleteTileResponse, GetTileDataByIdResponse, GetTilesByRectResponse,
    GetVersionResponse,
};

mod paths {
    use lazy_static::lazy_static;

    lazy_static! {
        pub static ref GLOBAL_REGEX_SET: regex::RegexSet = regex::RegexSet::new(vec![
            r"^/v1/tile/(?P<id>[^/?#]*)$",
            r"^/v1/tiles$",
            r"^/v1/tiles/findByRect$",
            r"^/v1/version$"
        ])
        .expect("Unable to create global regex set");
    }
    pub(crate) static ID_TILE_ID: usize = 0;
    lazy_static! {
        pub static ref REGEX_TILE_ID: regex::Regex =
            regex::Regex::new(r"^/v1/tile/(?P<id>[^/?#]*)$")
                .expect("Unable to create regex for TILE_ID");
    }
    pub(crate) static ID_TILES: usize = 1;
    pub(crate) static ID_TILES_FINDBYRECT: usize = 2;
    pub(crate) static ID_VERSION: usize = 3;
}

pub struct MakeService<T, C>
where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    api_impl: T,
    marker: PhantomData<C>,
}

impl<T, C> MakeService<T, C>
where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    pub fn new(api_impl: T) -> Self {
        MakeService {
            api_impl,
            marker: PhantomData,
        }
    }
}

impl<T, C, Target> hyper::service::Service<Target> for MakeService<T, C>
where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    type Response = Service<T, C>;
    type Error = crate::ServiceError;
    type Future = future::Ready<Result<Self::Response, Self::Error>>;

    fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        Poll::Ready(Ok(()))
    }

    fn call(&mut self, target: Target) -> Self::Future {
        futures::future::ok(Service::new(self.api_impl.clone()))
    }
}

fn method_not_allowed() -> Result<Response<Body>, crate::ServiceError> {
    Ok(Response::builder()
        .status(StatusCode::METHOD_NOT_ALLOWED)
        .body(Body::empty())
        .expect("Unable to create Method Not Allowed response"))
}

pub struct Service<T, C>
where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    api_impl: T,
    marker: PhantomData<C>,
}

impl<T, C> Service<T, C>
where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    pub fn new(api_impl: T) -> Self {
        Service {
            api_impl: api_impl,
            marker: PhantomData,
        }
    }
}

impl<T, C> Clone for Service<T, C>
where
    T: Api<C> + Clone + Send + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    fn clone(&self) -> Self {
        Service {
            api_impl: self.api_impl.clone(),
            marker: self.marker.clone(),
        }
    }
}

impl<T, C> hyper::service::Service<(Request<Body>, C)> for Service<T, C>
where
    T: Api<C> + Clone + Send + Sync + 'static,
    C: Has<XSpanIdString> + Send + Sync + 'static,
{
    type Response = Response<Body>;
    type Error = crate::ServiceError;
    type Future = ServiceFuture;

    fn poll_ready(&mut self, cx: &mut Context) -> Poll<Result<(), Self::Error>> {
        self.api_impl.poll_ready(cx)
    }

    fn call(&mut self, req: (Request<Body>, C)) -> Self::Future {
        async fn run<T, C>(
            mut api_impl: T,
            req: (Request<Body>, C),
        ) -> Result<Response<Body>, crate::ServiceError>
        where
            T: Api<C> + Clone + Send + 'static,
            C: Has<XSpanIdString> + Send + Sync + 'static,
        {
            let (request, context) = req;
            let (parts, body) = request.into_parts();
            let (method, uri, headers) = (parts.method, parts.uri, parts.headers);
            let path = paths::GLOBAL_REGEX_SET.matches(uri.path());

            match &method {
                // AddTile - POST /tiles
                &hyper::Method::POST if path.matched(paths::ID_TILES) => {
                    let boundary =
                        match swagger::multipart::form::boundary(&headers) {
                            Some(boundary) => boundary.to_string(),
                            None => return Ok(Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body(Body::from("Couldn't find valid multipart body".to_string()))
                                .expect(
                                    "Unable to create Bad Request response for incorrect boundary",
                                )),
                        };

                    // Form Body parameters (note that non-required body parameters will ignore garbage
                    // values, rather than causing a 400 response). Produce warning header and logs for
                    // any unused fields.
                    let result = body.into_raw();
                    match result.await {
                            Ok(body) => {
                                use std::io::Read;

                                // Read Form Parameters from body
                                let mut entries = match Multipart::with_body(&body.to_vec()[..], boundary).save().size_limit(327680000).temp() {
                                    SaveResult::Full(entries) => {
                                        entries
                                    },
                                    _ => {
                                        return Ok(Response::builder()
                                                        .status(StatusCode::BAD_REQUEST)
                                                        .body(Body::from(format!("Unable to process all message parts")))
                                                        .expect("Unable to create Bad Request response due to failure to process all message"))
                                    },
                                };
                                let field_tile_data = entries.fields.remove("tile_data");
                                let param_tile_data = match field_tile_data {
                                    Some(field) => {
                                        let mut reader = field[0].data.readable().expect("Unable to read field for tile_data");
                                    Some({
                                        let mut data = Vec::new();
                                        reader.read_to_end(&mut data).expect("Reading saved bytes should never fail");
                                        let tile_data_model = swagger::ByteArray(data);
                                        tile_data_model
                                    })
                                    },
                                    None => {
                                            None
                                    }
                                };
                                let result = api_impl.add_tile(
                                            param_tile_data,
                                        &context
                                    ).await;
                                let mut response = Response::new(Body::empty());
                                response.headers_mut().insert(
                                            HeaderName::from_static("x-span-id"),
                                            HeaderValue::from_str((&context as &dyn Has<XSpanIdString>).get().0.clone().to_string().as_str())
                                                .expect("Unable to create X-Span-ID header value"));

                                        match result {
                                            Ok(rsp) => match rsp {
                                                AddTileResponse::Added
                                                    (body)
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(200).expect("Unable to turn 200 into a StatusCode");
                                                    response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for ADD_TILE_ADDED"));
                                                    let body = serde_json::to_string(&body).expect("impossible to fail to serialize");
                                                    *response.body_mut() = Body::from(body);
                                                },
                                                AddTileResponse::InvalidInput
                                                => {
                                                    *response.status_mut() = StatusCode::from_u16(400).expect("Unable to turn 400 into a StatusCode");
                                                },
                                            },
                                            Err(_) => {
                                                // Application code returned an error. This should not happen, as the implementation should
                                                // return a valid response.
                                                *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                                                *response.body_mut() = Body::from("An internal error occurred");
                                            },
                                        }

                                        Ok(response)
                            },
                            Err(e) => Ok(Response::builder()
                                                .status(StatusCode::BAD_REQUEST)
                                                .body(Body::from(format!("Couldn't read multipart body")))
                                                .expect("Unable to create Bad Request response due to unable read multipart body")),
                        }
                }

                // DeleteTile - DELETE /tile/{id}
                &hyper::Method::DELETE if path.matched(paths::ID_TILE_ID) => {
                    // Path parameters
                    let path: &str = &uri.path().to_string();
                    let path_params = paths::REGEX_TILE_ID.captures(&path).unwrap_or_else(|| {
                        panic!(
                            "Path {} matched RE TILE_ID in set but failed match against \"{}\"",
                            path,
                            paths::REGEX_TILE_ID.as_str()
                        )
                    });

                    let param_id = match percent_encoding::percent_decode(path_params["id"].as_bytes()).decode_utf8() {
                    Ok(param_id) => match param_id.parse::<i64>() {
                        Ok(param_id) => param_id,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter id: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["id"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                    let result = api_impl.delete_tile(param_id, &context).await;
                    let mut response = Response::new(Body::empty());
                    response.headers_mut().insert(
                        HeaderName::from_static("x-span-id"),
                        HeaderValue::from_str(
                            (&context as &dyn Has<XSpanIdString>)
                                .get()
                                .0
                                .clone()
                                .to_string()
                                .as_str(),
                        )
                        .expect("Unable to create X-Span-ID header value"),
                    );

                    match result {
                        Ok(rsp) => match rsp {
                            DeleteTileResponse::TileNotFound => {
                                *response.status_mut() = StatusCode::from_u16(404)
                                    .expect("Unable to turn 404 into a StatusCode");
                            }
                        },
                        Err(_) => {
                            // Application code returned an error. This should not happen, as the implementation should
                            // return a valid response.
                            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                            *response.body_mut() = Body::from("An internal error occurred");
                        }
                    }

                    Ok(response)
                }

                // GetTileDataById - GET /tile/{id}
                &hyper::Method::GET if path.matched(paths::ID_TILE_ID) => {
                    // Path parameters
                    let path: &str = &uri.path().to_string();
                    let path_params = paths::REGEX_TILE_ID.captures(&path).unwrap_or_else(|| {
                        panic!(
                            "Path {} matched RE TILE_ID in set but failed match against \"{}\"",
                            path,
                            paths::REGEX_TILE_ID.as_str()
                        )
                    });

                    let param_id = match percent_encoding::percent_decode(path_params["id"].as_bytes()).decode_utf8() {
                    Ok(param_id) => match param_id.parse::<i64>() {
                        Ok(param_id) => param_id,
                        Err(e) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't parse path parameter id: {}", e)))
                                        .expect("Unable to create Bad Request response for invalid path parameter")),
                    },
                    Err(_) => return Ok(Response::builder()
                                        .status(StatusCode::BAD_REQUEST)
                                        .body(Body::from(format!("Couldn't percent-decode path parameter as UTF-8: {}", &path_params["id"])))
                                        .expect("Unable to create Bad Request response for invalid percent decode"))
                };

                    let result = api_impl.get_tile_data_by_id(param_id, &context).await;
                    let mut response = Response::new(Body::empty());
                    response.headers_mut().insert(
                        HeaderName::from_static("x-span-id"),
                        HeaderValue::from_str(
                            (&context as &dyn Has<XSpanIdString>)
                                .get()
                                .0
                                .clone()
                                .to_string()
                                .as_str(),
                        )
                        .expect("Unable to create X-Span-ID header value"),
                    );

                    match result {
                        Ok(rsp) => match rsp {
                            GetTileDataByIdResponse::ABinaryObjectOfTile(body) => {
                                *response.status_mut() = StatusCode::from_u16(200)
                                    .expect("Unable to turn 200 into a StatusCode");
                                response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/octet-stream")
                                                            .expect("Unable to create Content-Type header for GET_TILE_DATA_BY_ID_A_BINARY_OBJECT_OF_TILE"));
                                let body = body.0;
                                *response.body_mut() = Body::from(body);
                            }
                            GetTileDataByIdResponse::TileNotFound => {
                                *response.status_mut() = StatusCode::from_u16(404)
                                    .expect("Unable to turn 404 into a StatusCode");
                            }
                        },
                        Err(_) => {
                            // Application code returned an error. This should not happen, as the implementation should
                            // return a valid response.
                            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                            *response.body_mut() = Body::from("An internal error occurred");
                        }
                    }

                    Ok(response)
                }

                // GetTilesByRect - GET /tiles/findByRect
                &hyper::Method::GET if path.matched(paths::ID_TILES_FINDBYRECT) => {
                    // Query parameters (note that non-required or collection query parameters will ignore garbage values, rather than causing a 400 response)
                    let query_params =
                        form_urlencoded::parse(uri.query().unwrap_or_default().as_bytes())
                            .collect::<Vec<_>>();
                    let param_left = query_params
                        .iter()
                        .filter(|e| e.0 == "left")
                        .map(|e| e.1.to_owned())
                        .nth(0);
                    let param_left = match param_left {
                        Some(param_left) => {
                            let param_left = <i64 as std::str::FromStr>::from_str(&param_left);
                            match param_left {
                            Ok(param_left) => Some(param_left),
                            Err(e) => return Ok(Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body(Body::from(format!("Couldn't parse query parameter left - doesn't match schema: {}", e)))
                                .expect("Unable to create Bad Request response for invalid query parameter left")),
                        }
                        }
                        None => None,
                    };
                    let param_left = match param_left {
                    Some(param_left) => param_left,
                    None => return Ok(Response::builder()
                        .status(StatusCode::BAD_REQUEST)
                        .body(Body::from("Missing required query parameter left"))
                        .expect("Unable to create Bad Request response for missing query parameter left")),
                };
                    let param_right = query_params
                        .iter()
                        .filter(|e| e.0 == "right")
                        .map(|e| e.1.to_owned())
                        .nth(0);
                    let param_right = match param_right {
                        Some(param_right) => {
                            let param_right = <i64 as std::str::FromStr>::from_str(&param_right);
                            match param_right {
                            Ok(param_right) => Some(param_right),
                            Err(e) => return Ok(Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body(Body::from(format!("Couldn't parse query parameter right - doesn't match schema: {}", e)))
                                .expect("Unable to create Bad Request response for invalid query parameter right")),
                        }
                        }
                        None => None,
                    };
                    let param_right = match param_right {
                    Some(param_right) => param_right,
                    None => return Ok(Response::builder()
                        .status(StatusCode::BAD_REQUEST)
                        .body(Body::from("Missing required query parameter right"))
                        .expect("Unable to create Bad Request response for missing query parameter right")),
                };
                    let param_bottom = query_params
                        .iter()
                        .filter(|e| e.0 == "bottom")
                        .map(|e| e.1.to_owned())
                        .nth(0);
                    let param_bottom = match param_bottom {
                        Some(param_bottom) => {
                            let param_bottom = <i64 as std::str::FromStr>::from_str(&param_bottom);
                            match param_bottom {
                            Ok(param_bottom) => Some(param_bottom),
                            Err(e) => return Ok(Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body(Body::from(format!("Couldn't parse query parameter bottom - doesn't match schema: {}", e)))
                                .expect("Unable to create Bad Request response for invalid query parameter bottom")),
                        }
                        }
                        None => None,
                    };
                    let param_bottom = match param_bottom {
                    Some(param_bottom) => param_bottom,
                    None => return Ok(Response::builder()
                        .status(StatusCode::BAD_REQUEST)
                        .body(Body::from("Missing required query parameter bottom"))
                        .expect("Unable to create Bad Request response for missing query parameter bottom")),
                };
                    let param_top = query_params
                        .iter()
                        .filter(|e| e.0 == "top")
                        .map(|e| e.1.to_owned())
                        .nth(0);
                    let param_top = match param_top {
                        Some(param_top) => {
                            let param_top = <i64 as std::str::FromStr>::from_str(&param_top);
                            match param_top {
                            Ok(param_top) => Some(param_top),
                            Err(e) => return Ok(Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body(Body::from(format!("Couldn't parse query parameter top - doesn't match schema: {}", e)))
                                .expect("Unable to create Bad Request response for invalid query parameter top")),
                        }
                        }
                        None => None,
                    };
                    let param_top = match param_top {
                    Some(param_top) => param_top,
                    None => return Ok(Response::builder()
                        .status(StatusCode::BAD_REQUEST)
                        .body(Body::from("Missing required query parameter top"))
                        .expect("Unable to create Bad Request response for missing query parameter top")),
                };
                    let param_zoom_level = query_params
                        .iter()
                        .filter(|e| e.0 == "zoom_level")
                        .map(|e| e.1.to_owned())
                        .nth(0);
                    let param_zoom_level = match param_zoom_level {
                        Some(param_zoom_level) => {
                            let param_zoom_level =
                                <i32 as std::str::FromStr>::from_str(&param_zoom_level);
                            match param_zoom_level {
                            Ok(param_zoom_level) => Some(param_zoom_level),
                            Err(e) => return Ok(Response::builder()
                                .status(StatusCode::BAD_REQUEST)
                                .body(Body::from(format!("Couldn't parse query parameter zoom_level - doesn't match schema: {}", e)))
                                .expect("Unable to create Bad Request response for invalid query parameter zoom_level")),
                        }
                        }
                        None => None,
                    };

                    let result = api_impl
                        .get_tiles_by_rect(
                            param_left,
                            param_right,
                            param_bottom,
                            param_top,
                            param_zoom_level,
                            &context,
                        )
                        .await;
                    let mut response = Response::new(Body::empty());
                    response.headers_mut().insert(
                        HeaderName::from_static("x-span-id"),
                        HeaderValue::from_str(
                            (&context as &dyn Has<XSpanIdString>)
                                .get()
                                .0
                                .clone()
                                .to_string()
                                .as_str(),
                        )
                        .expect("Unable to create X-Span-ID header value"),
                    );

                    match result {
                        Ok(rsp) => match rsp {
                            GetTilesByRectResponse::AJSONArrayOfTilesWithInfo(body) => {
                                *response.status_mut() = StatusCode::from_u16(200)
                                    .expect("Unable to turn 200 into a StatusCode");
                                response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for GET_TILES_BY_RECT_AJSON_ARRAY_OF_TILES_WITH_INFO"));
                                let body = serde_json::to_string(&body)
                                    .expect("impossible to fail to serialize");
                                *response.body_mut() = Body::from(body);
                            }
                            GetTilesByRectResponse::InvalidParameterValues => {
                                *response.status_mut() = StatusCode::from_u16(400)
                                    .expect("Unable to turn 400 into a StatusCode");
                            }
                        },
                        Err(_) => {
                            // Application code returned an error. This should not happen, as the implementation should
                            // return a valid response.
                            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                            *response.body_mut() = Body::from("An internal error occurred");
                        }
                    }

                    Ok(response)
                }

                // GetVersion - GET /version
                &hyper::Method::GET if path.matched(paths::ID_VERSION) => {
                    let result = api_impl.get_version(&context).await;
                    let mut response = Response::new(Body::empty());
                    response.headers_mut().insert(
                        HeaderName::from_static("x-span-id"),
                        HeaderValue::from_str(
                            (&context as &dyn Has<XSpanIdString>)
                                .get()
                                .0
                                .clone()
                                .to_string()
                                .as_str(),
                        )
                        .expect("Unable to create X-Span-ID header value"),
                    );

                    match result {
                        Ok(rsp) => match rsp {
                            GetVersionResponse::ObjectWithCurrentVersionString(body) => {
                                *response.status_mut() = StatusCode::from_u16(200)
                                    .expect("Unable to turn 200 into a StatusCode");
                                response.headers_mut().insert(
                                                        CONTENT_TYPE,
                                                        HeaderValue::from_str("application/json")
                                                            .expect("Unable to create Content-Type header for GET_VERSION_OBJECT_WITH_CURRENT_VERSION_STRING"));
                                let body = serde_json::to_string(&body)
                                    .expect("impossible to fail to serialize");
                                *response.body_mut() = Body::from(body);
                            }
                        },
                        Err(_) => {
                            // Application code returned an error. This should not happen, as the implementation should
                            // return a valid response.
                            *response.status_mut() = StatusCode::INTERNAL_SERVER_ERROR;
                            *response.body_mut() = Body::from("An internal error occurred");
                        }
                    }

                    Ok(response)
                }

                _ if path.matched(paths::ID_TILE_ID) => method_not_allowed(),
                _ if path.matched(paths::ID_TILES) => method_not_allowed(),
                _ if path.matched(paths::ID_TILES_FINDBYRECT) => method_not_allowed(),
                _ if path.matched(paths::ID_VERSION) => method_not_allowed(),
                _ => Ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(Body::empty())
                    .expect("Unable to create Not Found response")),
            }
        }
        Box::pin(run(self.api_impl.clone(), req))
    }
}

/// Request parser for `Api`.
pub struct ApiRequestParser;
impl<T> RequestParser<T> for ApiRequestParser {
    fn parse_operation_id(request: &Request<T>) -> Option<&'static str> {
        let path = paths::GLOBAL_REGEX_SET.matches(request.uri().path());
        match request.method() {
            // AddTile - POST /tiles
            &hyper::Method::POST if path.matched(paths::ID_TILES) => Some("AddTile"),
            // DeleteTile - DELETE /tile/{id}
            &hyper::Method::DELETE if path.matched(paths::ID_TILE_ID) => Some("DeleteTile"),
            // GetTileDataById - GET /tile/{id}
            &hyper::Method::GET if path.matched(paths::ID_TILE_ID) => Some("GetTileDataById"),
            // GetTilesByRect - GET /tiles/findByRect
            &hyper::Method::GET if path.matched(paths::ID_TILES_FINDBYRECT) => {
                Some("GetTilesByRect")
            }
            // GetVersion - GET /version
            &hyper::Method::GET if path.matched(paths::ID_VERSION) => Some("GetVersion"),
            _ => None,
        }
    }
}
