use serde::{Deserialize, Serialize};
use std::ops::{Add, Mul, Sub};

#[derive(Serialize, Deserialize, Default, Copy, Clone, PartialOrd, PartialEq, Debug)]
pub struct Meter {
    pub value: f64,
}

impl Meter {
    pub fn new(value: f64) -> Self {
        Meter { value: value }
    }
}

// TODO impl Float

impl Add for Meter {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self {
            value: self.value + other.value,
        }
    }
}

impl Sub for Meter {
    type Output = Self;

    fn sub(self, other: Self) -> Self::Output {
        Self {
            value: self.value - other.value,
        }
    }
}

impl Mul for Meter {
    type Output = Self;

    fn mul(self, other: Self) -> Self::Output {
        Self {
            value: self.value * other.value,
        }
    }
}
